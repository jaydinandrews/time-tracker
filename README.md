## Time Tracking Application

This little web app allows users to input events and the time they spent on those events and the application will display there time spent in a donought pie chart.

### Setup

First, you'll need to install Django. The javascript and css for bootstrap are already downloaded so you wont need to worry about that. 
Next, you'll need to change/add a secret key in the ```mysite/settings.py``` file under ```SECRET_KEY=```. 
You also may want to add a new admin user (although it is not necessary) with ```python manage.py createsuperuser```.

### Using the application
Using the app is straightfoward, head over to *New Event* to add an event to the pie chart. Enter a title and the time you spent doing it as a floating point number.
Now you should be able to see your events on the main page. If you would like to delete an event go to *Remove Event* and you can remove it there. 
If you wish to start again from scratch, you can choose to *Delete All*.