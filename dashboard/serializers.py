from rest_framework.serializers import ModelSerializer
from rest_framework import serializers

from .models import Event

class EventSerializers(ModelSerializer):
    #title = serializers.CharField(max_length=150)
    #category = serializers.CharField(max_length=150)
    time_spent = serializers.FloatField()
    class Meta:
        model = Event
        fields = ['title', 'category', 'time_spent']