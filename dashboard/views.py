# from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from django.http import HttpResponseRedirect

from .models import Event
from .forms import EventForm
# from .serializers import EventSerializers

# from rest_framework.views import APIView
# from rest_framework.response import Response

class HomeView(View):
    def get(self, request, *args, **kwargs):
        data = Event.objects.all()
        colors = [
            'rgb(220,20,60)',
            'rgb(60,220,20)',
            'rgb(20,60,200)',
            'rgb(200,190,0)',
            'rgb(0,128,128)',
            'rgb(128,0,0)',
            'rgb(0,0,128)',
            'rgb(160, 0, 160)',
            'rgb(0, 160, 160)',
            'rgb(200, 160, 0)',
            'rgb(100,200,40)',
            'rgb(220,20,60)',
            'rgb(60,220,20)',
            'rgb(20,60,200)',
            'rgb(200,190,0)',
            'rgb(0,128,128)',
            'rgb(128,0,0)',
            'rgb(0,0,128)',
            'rgb(160, 0, 160)',
            'rgb(0, 160, 160)',
            'rgb(200, 160, 0)',
            'rgb(100,200,40)',
            'rgb(160, 0, 160)',
            'rgb(0, 160, 160)',
            'rgb(200, 160, 0)',
            'rgb(100,200,40)',
            'rgb(220,20,60)',
            'rgb(60,220,20)',
            'rgb(20,60,200)',
            'rgb(200,190,0)',
            'rgb(100,200,40)',
        ]
        return render(request, 'dashboard/home.html', {'data': data, 'colors': colors})







# class ChartData(APIView):
#     authentication_classes = []
#     permission_classes = []

#     def get(self, request, format=None):
#         #labels = ['Red','Yellow','Blue', 'Green']
#         #default_items = [10, 20, 30, 40]

#         events = Event.objects.all()
#         serializer = EventSerializers(events, many=True)
#         return Response(serializer.data)

def new_event(request):
    form = EventForm(request.POST or None) 
    if form.is_valid():
        instance = form.save(commit=True)
        instance.save()
        return HttpResponseRedirect("/")
    
    return render(request, "dashboard/new_event.html", {"form" : form })


def delete_event(request):
    data = Event.objects.all()
    return render(request, "dashboard/delete_event.html", {"data" : data})


def delete_it(request, id):
    if request.method == 'POST':
        Event.objects.get(pk=id).delete()
    return HttpResponseRedirect("/")

def delete_everything(request):
    if request.method == 'POST':
        Event.objects.all().delete()
    return HttpResponseRedirect("/")

def about(request):
    return render(request, "dashboard/about.html", {})