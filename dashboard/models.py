from django.db import models
from django.forms import ModelForm

class Event(models.Model):
    title = models.CharField(max_length=150)
    # category = models.CharField(max_length=150)
    time_spent = models.FloatField()

    def __str__(self):
        return self.title