from django.conf.urls import url, include
from .views import HomeView, new_event, delete_event, delete_it, delete_everything, about

urlpatterns = [
        url(r'^$', HomeView.as_view(), name='index'),
        url(r'^newEvent/$', new_event),
        url(r'^deleteEvent/$', delete_event),
        url(r'^delete/(?P<id>\d+)$', delete_it),
        url(r'^deleteEverything$', delete_everything),
        url(r'^about/$', about),
        ]